const express = require('express');
const app = express();
const port = 3000;
const multer = require('multer');
const upload = multer();
const jsonParser = (raw) => {
	const parsed = JSON.parse(raw);
	return typeof parsed === 'string' ? jsonParser(parsed) : parsed;
};
app.post(
	'/upload1',
	upload.single('avatar'),
	(req, res) => {
		const avatar = req.file;
		const infoJSON = jsonParser(req.body.info);
		console.log(avatar);
		res.json(infoJSON);
	}
);
app.post(
	'/upload2',
	upload.fields([
		{ name: 'avatar', maxCount: 1 },
		{ name: 'info', maxCount: 1 },
	]),
	(req, res) => {
		const avatar = req.files['avatar'][0];
		const infoJSON = JSON.parse(req.files['info'][0].buffer.toString('utf8'));
		console.log(avatar);
		res.json(infoJSON);
	}
);

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
